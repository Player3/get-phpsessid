# get-PHPSESSID

I'm sick of the amount of times I've had to google this during CTF's for burp interception, so I'm leaving it here for me to remember. 



## Script
```
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

getCookie('PHPSESSID');
```
